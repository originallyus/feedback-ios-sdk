
Pod::Spec.new do |s|
  s.name             = 'OUSFeedbackSDK'
  s.version          = '0.4.5'
  s.summary          = 'OUS Feedback SDK'
  s.description  = <<-DESC
An easy to use digital feedback solution.
                   DESC
  s.homepage     = "https://gitlab.com/originallyus/feedback-ios-sdk"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "author" => "torin@originally.us" }
  
  s.platform     = :ios, "10.0"
  s.ios.deployment_target  = '10.3'

  s.source       = { :git => "https://gitlab.com/originallyus/feedback-ios-sdk.git", :tag => s.version.to_s }

  s.source_files  = 'FeedbackSDKFramework/OUSFeedbackSDK/**/*.{h,m}'
  s.public_header_files = 'FeedbackSDKFramework/OUSFeedbackSDK/**/*.h'
  s.resource_bundles = {
     'OUSFeedbackSDK' => ['FeedbackSDKFramework/OUSFeedbackSDK/**/*.{xib,ttf,png,xyz}']
  }
  s.ios.exclude_files = 'Sources/Pods'

  s.requires_arc  = true
  s.framework     = "UIKit"

end

  
