//
//  ViewController.m
//

#import "ViewController.h"

@import OUSFeedbackSDK;

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Configure OUSFeedbackSDK to use this User ID in reports
    self.lblVersion.text = [NSString stringWithFormat:@"v%@", [OUSFeedbackSDK version]];
}


#pragma mark - Optional

- (IBAction)onBtnSetUserID:(id)sender
{
    //Obtain your app's UserID from somewhere else
    NSString * userId = @"1234";
    
    //Configure OUSFeedbackSDK to use this User ID in reports
    [OUSFeedbackSDK setUserId:userId];
}

- (IBAction)onBtnSetMetadata:(id)sender
{
    //Obtain this metadata from somewhere else
    NSString * metadata = @"policy_73719613";
    
    //Configure OUSFeedbackSDK to use this metadata in reports
    [OUSFeedbackSDK setMetadata:metadata];
}


#pragma mark - Language

- (IBAction)onBtnLanguageEnglish:(id)sender
{
    //Optional: set language
    //Note: this will requires translation texts to be already available on our backend
    [OUSFeedbackSDK setLanguage:@"en"];     //"en", "zh", "zh_tw", "ms", "id", "ta", "th", "tl", "vi", "ko"
}

- (IBAction)onBtnLanguageChinese:(id)sender
{
    //Optional: set language
    //Note: this will requires translation texts to be already available on our backend
    [OUSFeedbackSDK setLanguage:@"zh"];     //"en", "zh", "zh_tw", "ms", "id", "ta", "th", "tl", "vi", "ko"
}

- (IBAction)onBtnLanguageTraditionalChinese:(id)sender
{
    //Optional: set language
    //Note: this will requires translation texts to be already available on our backend
    [OUSFeedbackSDK setLanguage:@"zh_tw"];     //"en", "zh", "zh_tw", "ms", "id", "ta", "th", "tl", "vi", "ko"
}

- (IBAction)onBtnLanguageTamil:(id)sender
{
    //Optional: set language
    //Note: this will requires translation texts to be already available on our backend
    [OUSFeedbackSDK setLanguage:@"ta"];     //"en", "zh", "zh_tw", "ms", "id", "ta", "th", "tl", "vi", "ko"
}

- (IBAction)onBtnLanguageMalay:(id)sender
{
    //Optional: set language
    //Note: this will requires translation texts to be already available on our backend
    [OUSFeedbackSDK setLanguage:@"ms"];     //"en", "zh", "zh_tw", "ms", "id", "ta", "th", "tl", "vi", "ko"
}

- (IBAction)onBtnLanguageThai:(id)sender
{
    //Optional: set language
    //Note: this will requires translation texts to be already available on our backend
    [OUSFeedbackSDK setLanguage:@"th"];     //"en", "zh", "zh_tw", "ms", "id", "ta", "th", "tl", "vi", "ko"
}

- (IBAction)onBtnLanguageIndonesia:(id)sender
{
    //Optional: set language
    //Note: this will requires translation texts to be already available on our backend
    [OUSFeedbackSDK setLanguage:@"id"];     //"en", "zh", "zh_tw", "ms", "id", "ta", "th", "tl", "vi", "ko"
}

- (IBAction)onBtnLanguageTagalog:(id)sender
{
    //Optional: set language
    //Note: this will requires translation texts to be already available on our backend
    [OUSFeedbackSDK setLanguage:@"tl"];     //"en", "zh", "zh_tw", "ms", "id", "ta", "th", "tl", "vi", "ko"
}

- (IBAction)onBtnLanguageVietnamese:(id)sender
{
    //Optional: set language
    //Note: this will requires translation texts to be already available on our backend
    [OUSFeedbackSDK setLanguage:@"vi"];     //"en", "zh", "zh_tw", "ms", "id", "ta", "th", "tl", "vi", "ko"
}

- (IBAction)onBtnLanguageKorean:(id)sender
{
    //Optional: set language
    //Note: this will requires translation texts to be already available on our backend
    [OUSFeedbackSDK setLanguage:@"ko"];     //"en", "zh", "zh_tw", "ms", "id", "ta", "th", "tl", "vi", "ko"
}



#pragma mark - Test / Debug

- (IBAction)onBtnDebugFormSatisfaction:(id)sender
{
    //This will always show the rating form in DEBUG mode
    //Optional: eventTag is optional
    [OUSFeedbackSDK debugFeedbackFormWithEventTag:@"btn_debug" completion:^(BOOL didShow) {
        NSLog(@"didShow: %@", @(didShow));
    }];
    
    //Starting from SDK version 0.3.x onwards, there are 6 different Survey Type available, configurable from DFS CMS (backend)
    //Please consult your business/admin team to know exactly which eventTag, formSlug to be used at which point of your User Journey
    
    /* Sample code for force dismissing UI, to be used only for session timeout or session expired scenarios */
    /*
     * Forcefully dismiss the rating UI
     * This may be suitable for scenarios like session timeout, session expired where host application
     * needs to forcefully dismiss any user-related UI and logout the user
     * Note: This is not part of the initial UI/UX design and business requirement. This will interfere with the
     * showing and prompting frequency logics in the backend. Please consult your business team before deciding to use this
     * Completion block will be called as per usual after finish dismissing the UI
     * This function can be called from any thread (both background thread & main thread)
     */
    /*
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [OUSFeedbackSDK forceDismiss:YES];
    });
     */
}

- (IBAction)onBtnDebugFormNPS:(id)sender
{
    //This will always show the rating form in DEBUG mode
    //Optional: eventTag is optional
    [OUSFeedbackSDK debugFeedbackFormWithEventTag:@"btn_debug_alt" formSlug:@"nps-1"];
}

- (IBAction)onBtnDebugFormPoll:(id)sender
{
    //This will always show the rating form in DEBUG mode
    //Optional: eventTag is optional
    [OUSFeedbackSDK debugFeedbackFormWithEventTag:@"btn_debug_alt" formSlug:@"poll-1"];
}

- (IBAction)onBtnDebugFormComment:(id)sender
{
    //This will always show the rating form in DEBUG mode
    //Optional: eventTag is optional
    [OUSFeedbackSDK debugFeedbackFormWithEventTag:@"btn_debug_alt" formSlug:@"comment-1"];
}

- (IBAction)onBtnDebugFormCES:(id)sender
{
    //This will always show the rating form in DEBUG mode
    //Optional: eventTag is optional
    [OUSFeedbackSDK debugFeedbackFormWithEventTag:@"btn_debug_alt" formSlug:@"effort-1"];
}

- (IBAction)onBtnDebugFormExternal:(id)sender
{
    //This will always show the rating form in DEBUG mode
    //Optional: eventTag is optional
    [OUSFeedbackSDK debugFeedbackFormWithEventTag:@"btn_debug_alt" formSlug:@"external-1"];
}

- (IBAction)onBtnDebugFormContentUsefulness:(id)sender
{
    //This will always show the rating form in DEBUG mode
    //Optional: eventTag is optional
    [OUSFeedbackSDK debugFeedbackFormWithEventTag:@"btn_debug_alt" formSlug:@"content_usefulness-1"];
}


#pragma mark - Test / Production

- (IBAction)onBtnProductionFormSatisfaction:(id)sender
{
    //A form may not be shown all the time, depends on the frequency configurations on our SDK backend.
    //Optional: eventTag is optional
    [OUSFeedbackSDK showFeedbackFormWithEventTag:@"btn_show" completion:^(BOOL didShow) {
        NSLog(@"didShow: %@", @(didShow));
    }];
    
    //Starting from SDK version 0.3.x onwards, there are 6 different Survey Type available, configurable from DFS CMS (backend)
    //Please consult your business/admin team to know exactly which eventTag, formSlug to be used at which point of your User Journey
    //[OUSFeedbackSDK showFeedbackFormWithEventTag:@"btn_show_alt" formSlug:@"satisfaction-1"];
}

- (IBAction)onBtnProductionFormNPS:(id)sender
{
    //Optional: eventTag is optional
    [OUSFeedbackSDK showFeedbackFormWithEventTag:@"btn_show_alt" formSlug:@"nps-1"];
}

- (IBAction)onBtnProductionFormPoll:(id)sender
{
    //Optional: eventTag is optional
    [OUSFeedbackSDK showFeedbackFormWithEventTag:@"btn_show_alt" formSlug:@"poll-1"];
}

- (IBAction)onBtnProductionFormComment:(id)sender
{
    //Optional: eventTag is optional
    [OUSFeedbackSDK showFeedbackFormWithEventTag:@"btn_show_alt" formSlug:@"comment-1"];
}

- (IBAction)onBtnProductionFormCES:(id)sender
{
    //Optional: eventTag is optional
    [OUSFeedbackSDK showFeedbackFormWithEventTag:@"btn_show_alt" formSlug:@"effort-1"];
}

- (IBAction)onBtnProductionFormExternal:(id)sender
{
    //Optional: eventTag is optional
    [OUSFeedbackSDK showFeedbackFormWithEventTag:@"btn_show_alt" formSlug:@"external-1"];
}

- (IBAction)onBtnProductionFormContentUsefulness:(id)sender
{
    //Optional: eventTag is optional
    [OUSFeedbackSDK showFeedbackFormWithEventTag:@"btn_show_alt" formSlug:@"content_usefulness-1"];
}

@end
