//
//  OUSFeedbackAPI.h
//  FeedbackSDK
//
//  Created by Torin Nguyen on 23/4/20.
//  Copyright © 2021 Originally US. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OUSForm.h"

NS_ASSUME_NONNULL_BEGIN

@interface OUSFeedbackAPI : NSObject <NSURLSessionDelegate>

+ (NSString * _Nullable)appSecret;
+ (void)setAppSecret:(NSString * _Nonnull)appSecret;

+ (NSString * _Nullable)userId;
+ (void)setUserId:(NSString * _Nullable)userId;

+ (NSString * _Nullable)lang;
+ (void)setLanguage:(NSString * _Nullable)lang;


#pragma mark - Metadata

+ (void)clearMetadata;
+ (void)setMetadata:(NSString * _Nullable)value;
+ (void)setCustomMetadata:(NSString * _Nullable)key value:(NSString * _Nullable)value;


#pragma mark - API

+ (void)requestForm:(NSString * _Nullable)slug
           eventTag:(NSString * _Nullable)eventTag
              debug:(BOOL)debug
         completion:(void (^)(OUSForm * _Nullable model, NSError * _Nullable error))completionHandler;

+ (void)submitRating:(NSInteger)rating
            formSlug:(NSString * _Nullable)formSlug
            eventTag:(NSString * _Nullable)eventTag
           requestId:(id _Nullable)requestId
             options:(NSArray * _Nullable)optionSlugs
             comment:(NSString * _Nullable)comment
               debug:(BOOL)debug
          autoSubmit:(BOOL)isAutoSubmit
          completion:(void (^)(OUSForm * _Nullable model, NSError * _Nullable error))completionHandler;

//For delayed forms
+ (void)confirm_form_request:(NSString * _Nullable)slug
                    eventTag:(NSString * _Nullable)eventTag
                   requestId:(id _Nullable)requestId
                       debug:(BOOL)debug
                  completion:(void (^)(NSDictionary * _Nullable json, NSError * _Nullable error))completionHandler;

@end

NS_ASSUME_NONNULL_END
