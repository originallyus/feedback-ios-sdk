#import <Foundation/Foundation.h>

//
// Usage:
//    #include "OUSDeviceUUID.h"
//    NSString * uuid = [OUSDeviceUUID valueWithKeychain];
//

@interface OUSDeviceUUID : NSObject
{
}

+ (NSString *)valueWithKeychain;

@end
