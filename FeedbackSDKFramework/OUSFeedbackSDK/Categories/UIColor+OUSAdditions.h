#import <UIKit/UIKit.h>

@interface UIColor (OUSAdditions)

+ (UIColor * _Nullable)ous_colorWithHexString:(NSString * _Nonnull)hexString alpha:(CGFloat)alpha;
+ (UIColor * _Nullable)ous_colorWithHexString:(NSString * _Nonnull)hexString;


#pragma mark -

- (UIColor * _Nonnull)ous_lighterColor;
- (UIColor * _Nonnull)ous_darkerColor;

@end
