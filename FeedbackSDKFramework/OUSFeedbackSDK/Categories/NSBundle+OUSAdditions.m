#import "NSBundle+OUSAdditions.h"

@implementation NSBundle (OUSAdditions)

+ (NSBundle * _Nullable)ous_podBundleWithClass:(Class _Nonnull)classObject
{
    //Retrieve the current bundle used by our Pod (not the main bundle)
    NSBundle * bundle = [NSBundle bundleForClass:classObject];
    
    //Fallback 0: for static library, there is no bundle file/folder
    //The resources are located in here xxxxxxxxxxxx.app/Frameworks/OUSFeedbackSDK.framework
    //xxxxxxxxxxxx.app is where NSBundle * bundle is points to
    //Use NSURL to sanity check the existence of such framework folder
    NSString * bundlePath = [bundle bundlePath];
    NSURL * bundleUrl = [bundle URLForResource:@"OUSFeedbackSDK" withExtension:@"framework" subdirectory:@"Frameworks"];
    if (bundleUrl != nil) {
        bundlePath = [bundlePath stringByAppendingString:@"/Frameworks/OUSFeedbackSDK.framework"];
        bundle = [NSBundle bundleWithPath:bundlePath];
        return bundle;
    }
    
    //Fallback 1: search for Pod bundle inside main bundle
    bundleUrl = [bundle URLForResource:@"OUSFeedbackSDK" withExtension:@"bundle"];
    if (bundleUrl != nil) {
        NSBundle * tmpBundle = [NSBundle bundleWithURL:bundleUrl];
        if (tmpBundle)
            return tmpBundle;
    }
    
    //Fallback 2: https://stackoverflow.com/questions/15900717/bundle-from-static-library
    bundle = [NSBundle mainBundle];
    bundleUrl = [bundle URLForResource:@"OUSFeedbackSDK" withExtension:@"bundle"];
    if (bundleUrl != nil) {
        NSBundle * tmpBundle = [NSBundle bundleWithURL:bundleUrl];
        if (tmpBundle)
            return tmpBundle;
    }

    NSLog(@"FeedbackSDK Error: 876165");
    return bundle;
}

@end
