#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UIView (OUSAdditions)

@property (nonatomic) BOOL ous_visible;
@property (nonatomic) CGFloat ous_left;
@property (nonatomic) CGFloat ous_top;
@property (nonatomic) CGFloat ous_right;
@property (nonatomic) CGFloat ous_bottom;
@property (nonatomic) CGFloat ous_width;
@property (nonatomic) CGFloat ous_height;
@property (nonatomic) CGFloat ous_centerX;
@property (nonatomic) CGFloat ous_centerY;
@property (nonatomic, readonly) CGPoint ous_contentCenter;


#pragma mark -

- (void)ous_removeAllSubviews;
- (void)ous_bringToFront;


#pragma mark -

- (UIView * _Nullable)ous_firstResponder;
- (BOOL)ous_hasFirstResponder;


#pragma mark -

- (void)ous_roundCorner;
- (void)ous_roundCornerWithRadius:(CGFloat)radius;
- (void)ous_roundCorners:(UIRectCorner)corners withRadius:(CGFloat)radius;


#pragma mark - Animation

- (void)ous_addFadeTransitionWithDuration:(CGFloat)duration;

- (void)ous_fadeInWithDuration:(CGFloat)duration;
- (void)ous_fadeInWithDuration:(CGFloat)duration completion:(void (^ __nullable)(BOOL finished))completion;
- (void)ous_fadeInWithDuration:(CGFloat)duration delay:(CGFloat)delay;
- (void)ous_fadeInWithDuration:(CGFloat)duration delay:(CGFloat)delay completion:(void (^ __nullable)(BOOL finished))completion;

- (void)ous_fadeOutWithDuration:(CGFloat)duration;
- (void)ous_fadeOutWithDuration:(CGFloat)duration completion:(void (^ __nullable)(BOOL finished))completion;
- (void)ous_fadeOutWithDuration:(CGFloat)duration andRemoveFromSuperView:(BOOL)toBeRemoved completion:(void (^ __nullable)(BOOL finished))completion;
- (void)ous_fadeOutWithDuration:(CGFloat)duration delay:(CGFloat)delay;
- (void)ous_fadeOutWithDuration:(CGFloat)duration delay:(CGFloat)delay completion:(void (^ __nullable)(BOOL finished))completion;

- (void)ous_fadeToAlpha:(CGFloat)alpha;
- (void)ous_fadeToAlpha:(CGFloat)alpha withDuration:(CGFloat)duration;
- (void)ous_fadeToAlpha:(CGFloat)alpha completion:(void (^ __nullable)(BOOL finished))completion;
- (void)ous_fadeToAlpha:(CGFloat)alpha withDuration:(CGFloat)duration delay:(CGFloat)delay completion:(void (^ __nullable)(BOOL finished))completion;

- (void)ous_animateTopToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay;
- (void)ous_animateBottomToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay;
- (void)ous_animateLeftToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay;
- (void)ous_animateRightToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay;
- (void)ous_animateWidthToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay;
- (void)ous_animateHeightToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay;
- (void)ous_animateCenterXToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay;
- (void)ous_animateCenterYToValue:(CGFloat)val duration:(CGFloat)duration delay:(CGFloat)delay;
- (void)ous_animateFrameToValue:(CGRect)val duration:(CGFloat)duration delay:(CGFloat)delay;


#pragma mark -

- (UITapGestureRecognizer * _Nullable)ous_addTapGestureWithTarget:(id _Nonnull)target action:(nullable SEL)action;


#pragma mark - Shake animation

- (void)ous_shakeX;
- (void)ous_shakeXWithOffset:(CGFloat)aOffset breakFactor:(CGFloat)aBreakFactor duration:(CGFloat)aDuration maxShakes:(NSInteger)maxShakes;

@end
