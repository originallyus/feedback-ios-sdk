#import <Foundation/Foundation.h>

@interface NSDate (OUSAdditions)

+ (void)ous_updateTimeFromServer:(NSTimeInterval)timestamp;
+ (NSTimeInterval)ous_adjustedServerTimestamp;

@end
