//
//  OUSFeedbackSDK.m
//

#import <CoreText/CoreText.h>
#import "OUSFeedbackSDK.h"
#import "OUSFeedbackAPI.h"
#import "OUSFeedbackViewController.h"
#import "NSString+OUSAdditions.h"
#import "NSBundle+OUSAdditions.h"
#import "NSDate+OUSAdditions.h"
#import "OUSForm.h"

#define OUS_FEEDBACK_SDK_VERSION                @"0.4.5"
#define OUS_FEEDBACK_SDK_ERROR_TITLE            @"FeedbackSDK Error"
#define OUS_FEEDBACK_SDK_DEFAULT_SLUG           @"native_rating_form"

@interface OUSFeedbackSDK () <OUSFeedbackInternalDelegate>
@property (nonatomic, strong) OUSFeedbackBaseVC * currentVC;

@property (nonatomic, strong) NSTimer * delay_timer;
@property (nonatomic, strong) OUSForm * delayed_form;
@property (nonatomic, strong) OUSFeedbackSDKCompletionBlock delayed_completion;
@property (nonatomic, assign) NSTimeInterval showingTimestamp;
@property (nonatomic, assign) NSTimeInterval expiryTimestamp;
@end

@implementation OUSFeedbackSDK

static NSNumber * isInitialized = nil;
static BOOL alwaysOnTop = YES;

+ (OUSFeedbackSDK *)sharedInstance
{
    static dispatch_once_t pred;
    static id __singleton = nil;
    dispatch_once(&pred, ^{
        __singleton = [[self alloc] init];
    });
    return __singleton;
}


#pragma mark - Basic Interfaces

+ (NSString *)version
{
    return OUS_FEEDBACK_SDK_VERSION;
}

+ (BOOL)isShowing
{
    return [self sharedInstance].currentVC != nil;
}

+ (void)forceDismiss:(BOOL)animated
{
    //internal_formDidDismissed delegate will handle the clean up

    //Make sure this is running on main thread
    if ([NSThread isMainThread]) {
        [[self sharedInstance].currentVC forceDismiss:animated];
        return;
    }
    
    //Switch to main thread
    OUS_WEAK_SELF
    dispatch_async(dispatch_get_main_queue(), ^{
        OUS_STRONG_SELF
        [[strongSelf sharedInstance].currentVC forceDismiss:animated];
    });
}


#pragma mark - Initialization

+ (void)initWithAppSecret:(NSString *)appSecret application:(UIApplication *)application
{
    if (!appSecret)
        appSecret = @"";
    
    //Reset
    [OUSFeedbackAPI setLanguage:nil];
    [OUSFeedbackAPI setUserId:nil];
    
    //Store app secret
    [OUSFeedbackAPI setAppSecret:appSecret];
    
    //Load custom fonts
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^{
        [self loadCustomFonts];
    });
    
    isInitialized = @YES;
}

+ (void)setUserId:(NSString *)userId
{
    [OUSFeedbackAPI setUserId:userId];
}

+ (NSString *)language
{
    return [OUSFeedbackAPI lang];
}

+ (void)setLanguage:(NSString *)lang
{
    [OUSFeedbackAPI setLanguage:lang];
}

+ (void)alwaysOnTop:(BOOL)enabled
{
    alwaysOnTop = enabled;
}


#pragma mark - Metadata

+ (void)clearMetadata
{
    [OUSFeedbackAPI clearMetadata];
}

+ (void)setMetadata:(NSString * _Nullable)value
{
    [OUSFeedbackAPI setMetadata:value];
}

+ (void)setCustomMetadata:(NSString * _Nullable)key value:(NSString * _Nullable)value
{
    [OUSFeedbackAPI setCustomMetadata:key value:value];
}


#pragma mark - Presentation

+ (void)debugFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
{
    [self debugFeedbackFormWithEventTag:eventTag formSlug:nil completion:nil];
}

+ (void)debugFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
                           completion:(OUSFeedbackSDKCompletionBlock _Nullable)completion
{
    [self debugFeedbackFormWithEventTag:eventTag formSlug:nil completion:completion];
}

+ (void)debugFeedbackFormWithEventTag:(NSString * _Nullable)eventTag formSlug:(NSString * _Nullable)formSlug
{
    [self debugFeedbackFormWithEventTag:eventTag formSlug:formSlug completion:nil];
}

+ (void)debugFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
                             formSlug:(NSString * _Nullable)formSlug
                           completion:(OUSFeedbackSDKCompletionBlock _Nullable)completion
{
    OUS_PREVENT_DOUBLE_TOUCH(1);
    
    //Checking whether the SDK has been initialized before
    BOOL isInitialized = [self checkInitialized];
    if (!isInitialized) {
        [self showAlertWithTitle:OUS_FEEDBACK_SDK_ERROR_TITLE
                         message:@"FeedbackSDK has not been initialized with an App Secret (Error code: 2814)!"];
        if (completion)
            completion(NO);
        return;
    }
    
    //Checking whether the SDK is being shown
    BOOL isShowing = [self isShowing];
    if (isShowing) {
        if (completion)
            completion(NO);
        return;
    }
        
    //Optional: override default formSlug
    if (formSlug == nil || formSlug.ous_trim.length <= 0)
        formSlug = OUS_FEEDBACK_SDK_DEFAULT_SLUG;

    //Call API to request for form display
    OUS_WEAK_SELF
    [OUSFeedbackAPI requestForm:formSlug
                       eventTag:eventTag
                          debug:YES
                     completion:^(OUSForm * _Nullable model, NSError * _Nullable error) {
        OUS_STRONG_SELF
        
        //Invalid model, possible wrong format or network error
        //Don't show error alert
        if (model == nil) {
            NSLog(@"No form to be displayed in debugFeedbackFormFrom: %@", error);
            if (completion)
                completion(NO);
            return;
        }
        
        //Business logic error
        if (model.isError) {
            [self showAlertWithTitle:OUS_FEEDBACK_SDK_ERROR_TITLE message:model.error];
            return;
        }
        
        //No form to show, on purpose
        if (model.hasValidForm == NO) {
            NSLog(@"No form to be displayed in showFeedbackFormFrom");
            if (completion)
                completion(NO);
        }
        
        //Show it in UI
        [strongSelf showFormModel:model completion:completion];
    }];
}

+ (void)showFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
{
    [self showFeedbackFormWithEventTag:eventTag formSlug:nil completion:nil];
}

+ (void)showFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
                          completion:(OUSFeedbackSDKCompletionBlock _Nullable)completion
{
    [self showFeedbackFormWithEventTag:eventTag formSlug:nil completion:completion];
}

+ (void)showFeedbackFormWithEventTag:(NSString * _Nullable)eventTag formSlug:(NSString * _Nullable)formSlug
{
    [self showFeedbackFormWithEventTag:eventTag formSlug:formSlug completion:nil];
}

+ (void)showFeedbackFormWithEventTag:(NSString * _Nullable)eventTag
                            formSlug:(NSString * _Nullable)formSlug
                          completion:(OUSFeedbackSDKCompletionBlock _Nullable)completion
{
    OUS_PREVENT_DOUBLE_TOUCH(1);
    
    //Checking whether the SDK has been initialized before
    BOOL isInitialized = [self checkInitialized];
    if (!isInitialized) {
        if (completion)
            completion(NO);
        return;
    }
    
    //Checking whether the SDK is being shown
    BOOL isShowing = [self isShowing];
    if (isShowing) {
        if (completion)
            completion(NO);
        return;
    }
        
    //Optional: override default formSlug
    if (formSlug == nil || formSlug.ous_trim.length <= 0)
        formSlug = OUS_FEEDBACK_SDK_DEFAULT_SLUG;
    
    //Call API to request for display
    OUS_WEAK_SELF
    [OUSFeedbackAPI requestForm:formSlug
                           eventTag:eventTag
                              debug:NO
                         completion:^(OUSForm * _Nullable model, NSError * _Nullable error) {
        OUS_STRONG_SELF

        //Invalid model, possible wrong format or network error
        //Don't show error alert
        if (model == nil) {
            if (error)
                NSLog(@"No form to be displayed in showFeedbackFormFrom: %@", error);
            if (completion)
                completion(NO);
            return;
        }
        
        //Critical error, possibly due to wrong config
        if (model.isError) {
            NSLog(@"No form to be displayed in showFeedbackFormFrom: %@", model.error);
            return;
        }
        
        //No form to show, on purpose
        if (model.hasValidForm == NO) {
            NSLog(@"No form to be displayed in showFeedbackFormFrom");
            if (completion)
                completion(NO);
            return;
        }
        
        //Show it in UI
        [strongSelf showFormModel:model completion:completion];
    }];
}



#pragma mark - Helpers

+ (BOOL)checkInitialized
{
    NSString * appSecret = [OUSFeedbackAPI appSecret];
    if (appSecret.ous_trim.length < 8)
    {
        NSLog(@"[FeedbackSDK] FeedbackSDK has not been initialized with an invalid App Secret (Error code: 2815)");
        return NO;
    }
    
    //Automatically initialize the SDK
    if (NO == isInitialized.boolValue)
    {
        NSLog(@"[FeedbackSDK] Please initialize FeedbackSDK with initWithAppSecret:application: function (Error code: 2816)");
        return NO;
    }
    
    return YES;
}

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController * alertVC = [UIAlertController alertControllerWithTitle:title
                                                                      message:message
                                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertVC addAction:defaultAction];
    
    UIWindow * alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    alertWindow.rootViewController = [[UIViewController alloc] init];

    id<UIApplicationDelegate> delegate = [UIApplication sharedApplication].delegate;
    // Applications that does not load with UIMainStoryboardFile might not have a window property:
    if ([delegate respondsToSelector:@selector(window)]) {
        // we inherit the main window's tintColor
        alertWindow.tintColor = delegate.window.tintColor;
    }

    // window level is above the top window (this makes the alert, if it's a sheet, show over the keyboard)
    UIWindow * topWindow = [UIApplication sharedApplication].windows.lastObject;
    alertWindow.windowLevel = topWindow.windowLevel + 1;

    [alertWindow makeKeyAndVisible];
    [alertWindow.rootViewController presentViewController:alertVC animated:YES completion:nil];
}

+ (UIWindow *)frontWindow
{
#if defined(SV_APP_EXTENSIONS)
    return nil;
#endif
    
    NSEnumerator * frontToBackWindows = [UIApplication.sharedApplication.windows reverseObjectEnumerator];
    for (UIWindow * window in frontToBackWindows)
    {
        BOOL windowOnMainScreen = window.screen == UIScreen.mainScreen;
        BOOL windowIsVisible = !window.hidden && window.alpha > 0;
        BOOL windowLevelSupported = window.windowLevel >= UIWindowLevelNormal && window.windowLevel <= UIWindowLevelNormal;        //UIWindowLevelAlert
        
        if (windowOnMainScreen && windowIsVisible && windowLevelSupported)
            return window;
    }
    return nil;
}

+ (void)showFormModel:(OUSForm *)model completion:(OUSFeedbackSDKCompletionBlock)completion
{
    //Just to be sure again
    //No valid form to show
    if (model.hasValidForm == NO) {
        NSLog(@"No valid form to be displayed in showFormModel");
        return;
    }
    
    //Make sure this is running on main thread
    if ([NSThread isMainThread]) {
        [[self sharedInstance] internal_showFormModel:model completion:completion];
        return;
    }
    
    //Switch to main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self sharedInstance] internal_showFormModel:model completion:completion];
    });
}


#pragma mark - Show form

//Check for delay before presenting the UI
- (void)internal_showFormModel:(OUSForm *)model completion:(OUSFeedbackSDKCompletionBlock)completion
{
    //Already showing
    if (self.currentVC != nil)
        return;
        
    //Immediate show the form
    if (model.hasDelay == NO) {
        [self internal_presentFormUiWithModel:model completion:completion];
        return;
    }
    
    //Already waiting for a delayed timer
    if (self.delay_timer != nil)
        return;
    
    //Update timestamps
    NSTimeInterval serverTimestamp = [NSDate ous_adjustedServerTimestamp];
    self.showingTimestamp = serverTimestamp + [model.delay_second floatValue];
    self.expiryTimestamp = serverTimestamp + [model.delay_second_expiry floatValue];
    
    //Keep a strong copy
    self.delayed_completion = completion;
    self.delayed_form = model;
    
    //Start the timer
    self.delay_timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                        target:self
                                                      selector:@selector(onDelayTimer:)
                                                      userInfo:nil
                                                       repeats:YES];
}

//Prevent the form UI directly
- (void)internal_presentFormUiWithModel:(OUSForm *)model completion:(OUSFeedbackSDKCompletionBlock)completion
{
    //Already showing
    if (self.currentVC != nil)
        return;
        
    OUSFeedbackViewController * vc = [[OUSFeedbackViewController alloc] initWithNib];
    vc.completionBlock = completion;
    vc.delegate = self;
    vc.model = model;
    
    //Keep a strong reference
    self.currentVC = vc;
    
    //Floating subview in main window
    UIView * windowView = [[self class] frontWindow];
    [windowView addSubview:vc.view];
    [windowView bringSubviewToFront:vc.view];
    vc.view.frame = windowView.bounds;
    vc.view.autoresizingMask = AUTORESIZING_MASK_WIDTH_HEIGHT;
    [vc introAnimation:nil];

    //KVO
    [windowView.layer addObserver:self forKeyPath:@"sublayers" options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (alwaysOnTop == NO)
        return;
    
    if (self.currentVC == nil || self.currentVC.view == nil)
        return;
    
    [self.currentVC.view.superview bringSubviewToFront:self.currentVC.view];
}


#pragma mark - Delay Timer

- (void)onDelayTimer:(NSTimer *)timer
{
    NSTimeInterval serverTimestamp = [NSDate ous_adjustedServerTimestamp];
    
    //Expired
    if (serverTimestamp > self.expiryTimestamp) {
        [self.delay_timer invalidate];
        self.delay_timer = nil;
        self.delayed_form = nil;
        self.delayed_completion = nil;
        return;
    }
    
    //Not yet reach delay time
    if (serverTimestamp < self.showingTimestamp)
        return;
    
    //Call API to confirm showing the form (fire and forget)
    [OUSFeedbackAPI confirm_form_request:self.delayed_form.slug
                                eventTag:self.delayed_form.event_tag
                               requestId:self.delayed_form.request_id ? self.delayed_form.request_id : self.delayed_form.ID
                                   debug:self.delayed_form.debug
                              completion:^(NSDictionary * _Nullable json, NSError * _Nullable error) {
        
    }];

    //Delay time reached, presenting the UI
    [self internal_presentFormUiWithModel:self.delayed_form completion:self.delayed_completion];
    [self.delay_timer invalidate];
    self.delay_timer = nil;
    self.delayed_form = nil;
    self.delayed_completion = nil;
}


#pragma mark - Custom Fonts

+ (void)loadCustomFonts
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //use filenames, not PostScript names
        [self custom_loadFontWithName:@"AIAEverest-Bold"];                  //PostScript name: "AIAEverestBold"     //actually missing '-'
        [self custom_loadFontWithName:@"AIAEverest-Medium"];                //PostScript name: same as filename
        [self custom_loadFontWithName:@"AIAEverest-Regular"];               //PostScript name: same as filename
        [self custom_loadFontWithName:@"AIAEverest-CondensedMedium"];       //PostScript name: same as filename
        
        [self custom_loadFontWithName:@"OpenSans-Bold"];                    //PostScript name: same as filename
        [self custom_loadFontWithName:@"OpenSans-Light"];                   //PostScript name: same as filename
        [self custom_loadFontWithName:@"OpenSans-Regular"];                 //PostScript name: same as filename
        [self custom_loadFontWithName:@"OpenSans-SemiBold"];                //PostScript name: same as filename
    });
}

+ (void)custom_loadFontWithName:(NSString *)fontFileName
{
    NSBundle * bundle = [NSBundle ous_podBundleWithClass:self.classForCoder];
    NSString * fontPath = [bundle pathForResource:fontFileName ofType:@"ttf"];
    if (fontPath == nil)
        fontPath = [bundle pathForResource:fontFileName ofType:@"otf"];
    if (fontPath == nil) {
        NSLog(@"Font name '%@' not found in custom framework", fontFileName);
        return;
    }
    
    NSData * fontData = [NSData dataWithContentsOfFile:fontPath];
    if (!fontData)
        return;
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)fontData);
    if (!provider)
        return;
    
    CGFontRef font = CGFontCreateWithDataProvider(provider);
    if (font)
    {
        CFErrorRef error = NULL;
        if (CTFontManagerRegisterGraphicsFont(font, &error) == NO)
        {
            CFStringRef errorDescription = CFErrorCopyDescription(error);
            NSLog(@"Failed to load font: %@", errorDescription);
            CFRelease(errorDescription);
        }
        CFRelease(font);
    }
    CFRelease(provider);
}


#pragma mark - OUSFeedbackInternalDelegate

- (void)internal_formDidDismissed:(UIViewController * _Nonnull)viewController
{
    //Release reference
    self.currentVC = nil;
    
    //Remove KVO observer
    @try{
        [[[self class] frontWindow] removeObserver:self forKeyPath:@"sublayers" context:NULL];
    }
    @catch(id anException){
        //Do nothing here
    }
}

@end
