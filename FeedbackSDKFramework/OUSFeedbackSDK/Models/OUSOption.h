//
//  OUSOption.h
//  OUSFeedbackSDK
//
//  Created by Torin Nguyen on 1/8/21.
//  Copyright © 2021 Originally US. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OUSBaseModel.h"

@interface OUSOption : OUSBaseModel

@property (nonatomic, copy) NSString * _Nullable slug;
@property (nonatomic, copy) NSString * _Nullable title;

@end
