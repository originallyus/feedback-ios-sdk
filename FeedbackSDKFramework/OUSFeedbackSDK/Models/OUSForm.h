//
//  Form.h
//  FeedbackSDK
//
//  Created by Torin Nguyen on 24/4/20.
//  Copyright © 2021 Originally US. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "OUSBaseModel.h"
#import "OUSTheme.h"

@interface OUSForm : OUSBaseModel

@property (nonatomic, assign) BOOL debug;
@property (nonatomic, copy) NSString * _Nullable slug;
@property (nonatomic, copy) NSString * _Nullable event_tag;
@property (nonatomic, copy) NSNumber * _Nullable request_id;

//Main styles
@property (nonatomic, copy) NSNumber * _Nullable instrusive_style;        //0: non-instrusive; 1: instrusive; 2: inline
@property (nonatomic, copy) NSString * _Nullable alignment;               //"center",

@property (nonatomic, copy) NSString * _Nullable image_file_url;          //https://aia-dfs.originally.us/uploads/icons/success.png

//Text
@property (nonatomic, copy) NSString * _Nullable title;                   //"HELP US GIVE YOU A BETTER EXPERIENCE"
@property (nonatomic, copy) NSString * _Nullable question;                //"Tell us about your experience today"
@property (nonatomic, copy) NSString * _Nullable instruction;             //"Please let us know what went wrong?"
@property (nonatomic, copy) NSString * _Nullable instruction_5_star;      //"Please let us know what went wrong?"
@property (nonatomic, copy) NSString * _Nullable secondary_question;      //"What is the primary reason for your score"
@property (nonatomic, copy) NSString * _Nullable secondary_question_5_star;
@property (nonatomic, copy) NSString * _Nullable comment_placeholder;     //"Let us know how we can make it better for you."
@property (nonatomic, copy) NSString * _Nullable comment_placeholder_5_star;
@property (nonatomic, copy) NSString * _Nullable fineprint;               //"(AIA may reach out to understand the feedback provided)"
@property (nonatomic, copy) NSString * _Nullable button_text;             //"Submit"
@property (nonatomic, copy) NSString * _Nullable url;                     //external survey

//Show/hide flags
@property (nonatomic, copy) NSNumber * _Nullable read_only_rating;        //1-5 or 10
@property (nonatomic, copy) NSString * _Nullable show_rating_star;        //1,
@property (nonatomic, copy) NSString * _Nullable show_rating_number;      //0,
@property (nonatomic, copy) NSString * _Nullable show_comment;            //BOOL
@property (nonatomic, copy) NSNumber * _Nullable comment_mandatory;       //BOOL

//Rating number 1-10
@property (nonatomic, copy) NSNumber * _Nullable rating_min;              //1
@property (nonatomic, copy) NSNumber * _Nullable rating_max;              //5

//Options
@property (nonatomic, copy) NSNumber * _Nullable options_randomize;       //BOOL
@property (nonatomic, copy) NSString * _Nullable options_multiselect;     //BOOL

//Splash screen 2024
@property (nonatomic, copy) NSString * _Nullable splash_screen_title;
@property (nonatomic, copy) NSString * _Nullable splash_screen_question;
@property (nonatomic, copy) NSString * _Nullable splash_screen_button_proceed;
@property (nonatomic, copy) NSString * _Nullable splash_screen_button_skip;

//Content Usefulness
@property (nonatomic, copy) NSString * _Nullable positive_feedback_button;
@property (nonatomic, copy) NSString * _Nullable negative_feedback_button;

//Delay feature
@property (nonatomic, copy) NSNumber * _Nullable delay_second;
@property (nonatomic, copy) NSNumber * _Nullable delay_second_expiry;     //default: 120s

//Linked survey
@property (nonatomic, copy) NSString * _Nullable action;                  //default: nil or 'dismiss'
@property (nonatomic, copy) NSNumber * _Nullable linked_survey_id;        //default: nil

//Debug
@property (nonatomic, copy) NSString * _Nullable error;

//Dynamic property
@property (nonatomic, strong) OUSTheme * _Nullable theme;
@property (nonatomic, strong) OUSForm * _Nullable extra_form;
@property (nonatomic, strong) NSMutableArray * _Nullable options;

//Success
@property (nonatomic, strong) NSArray * _Nullable auto_submit;            // [1,2,3,...10]
@property (nonatomic, copy) NSNumber * _Nullable auto_store_review;       //BOOL
@property (nonatomic, copy) NSString * _Nullable ios_review_url;
@property (nonatomic, strong) NSArray * _Nullable preload_images;         // [ "https://xxx", "https://yyy" ]



#pragma mark -

- (BOOL)isError;
- (BOOL)hasValidForm;

- (BOOL)isNonIntrusive;
- (BOOL)isIntrusive;
- (BOOL)isInline;       //To be supported, fallback to non-instrusive

- (BOOL)hasRating;
- (BOOL)hasOptions;
- (BOOL)hasRatingOrOption;
- (BOOL)hasSplashScreen;
- (BOOL)hasDelay;

//Content usefulness
- (BOOL)hasDualButtons;

//For linked survey
- (BOOL)isActionDismiss;
- (BOOL)isActionLinkedSurvey;

- (NSTextAlignment)text_alignment;

- (BOOL)hasAutoSubmitForRating:(NSInteger)rating;

@end
