//
//  Form.m
//  FeedbackSDK
//
//  Created by Torin Nguyen on 24/4/20.
//  Copyright © 2021 Originally US. All rights reserved.
//

#import "OUSForm.h"
#import "OUSOption.h"
#import "NSString+OUSAdditions.h"

@implementation OUSForm

//Override
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super initWithDictionary:dict];
    if (!self)
        return nil;
 
    //Custom handling for request_id
    self.request_id = self.ID;
    
    //Theme
    NSDictionary * themeDict = [dict objectForKey:@"theme"];
    if ([themeDict isKindOfClass:NSDictionary.class])
        self.theme = [[OUSTheme alloc] initWithDictionary:themeDict];
    
    //Options
    NSMutableArray * optionsArrayModel = [NSMutableArray array];
    NSArray * optionsArray = [dict objectForKey:@"options"];
    if ([optionsArray isKindOfClass:[NSArray class]])
        for (NSDictionary * optionDict in optionsArray)
            if ([optionDict isKindOfClass:NSDictionary.class]) {
                OUSOption * optionModel = [[OUSOption alloc] initWithDictionary:optionDict];
                if (optionModel)
                    [optionsArrayModel addObject:optionModel];
        }
    
    //Randomize the options order
    //Do this only once to keep it stable for the rest of the code
    if (self.options_randomize.boolValue) {
        NSUInteger count = [optionsArrayModel count];
        for (NSUInteger i = 0; i < count; ++i) {
            NSUInteger nElements = count - i;
            NSUInteger n = arc4random_uniform((u_int32_t)nElements) + i;
            [optionsArrayModel exchangeObjectAtIndex:i withObjectAtIndex:n];        // Select a random element to swap with
        }
    }
    
    //Assign
    self.options = optionsArrayModel;
    
    //Intermediate form
    NSDictionary * extra_form = [dict objectForKey:@"extra_form"];
    if ([extra_form isKindOfClass:NSDictionary.class])
        self.extra_form = [[OUSForm alloc] initWithDictionary:extra_form];
    
    return self;
}


#pragma mark -

- (BOOL)isError
{
    if (self.error.length > 2)
        return YES;
    if (self.error != nil && [self.error isKindOfClass:NSString.class] == NO) {
        NSLog(@"!!!!%@.error is not NSString (%@)!!!!", self.class, self.error.class);
        return NO;
    }
    return NO;
}

- (BOOL)hasValidForm
{
    if (self.title.length <= 0)
        return NO;
    if (self.theme == nil)
        return NO;
    
    return YES;
}

- (BOOL)isNonIntrusive
{
    return self.instrusive_style.integerValue != 1;
}
- (BOOL)isIntrusive
{
    return self.instrusive_style.integerValue == 1;
}
//To be supported
- (BOOL)isInline
{
    return self.instrusive_style.integerValue == 2;
}

- (BOOL)hasRating
{
    return self.show_rating_star.boolValue || self.show_rating_number.boolValue;
}

- (BOOL)hasOptions
{
    return self.options.count > 0;
}

- (BOOL)hasRatingOrOption
{
    return self.show_rating_star.boolValue || self.show_rating_number.boolValue || self.options.count > 0;
}

- (BOOL)hasSplashScreen
{
    return self.splash_screen_title.length > 0 && self.splash_screen_question.length > 0 && self.splash_screen_button_skip.length > 0 && self.splash_screen_button_proceed.length > 0;
}

- (BOOL)hasDelay
{
    return self.delay_second != nil && self.delay_second.intValue > 0;
}

- (BOOL)hasDualButtons
{
    return self.positive_feedback_button.length > 0 && self.negative_feedback_button.length > 0;
}

- (BOOL)isActionDismiss
{
    return [self.action ous_isEqualToStringIgnoreCase:@"dismiss"];
}

- (BOOL)isActionLinkedSurvey
{
    return [self.action ous_isEqualToStringIgnoreCase:@"linked_survey"] || [self.linked_survey_id integerValue] > 0;
}

- (BOOL)hasAutoSubmitForRating:(NSInteger)rating
{
    if (self.auto_submit == nil || [self.auto_submit isKindOfClass:NSArray.class] == NO)
        return NO;
    
    for (NSNumber * num in self.auto_submit)
        if (num.integerValue == rating)
            return YES;
    
    return NO;
}

- (NSTextAlignment)text_alignment
{
    if ([self.alignment ous_isEqualToStringIgnoreCase:@"left"])
        return NSTextAlignmentLeft;
    if ([self.alignment ous_isEqualToStringIgnoreCase:@"right"])
        return NSTextAlignmentRight;
    if ([self.alignment ous_isEqualToStringIgnoreCase:@"center"])
        return NSTextAlignmentCenter;
    
    return NSTextAlignmentNatural;
}

@end
